package main

import (
	"bufio"
	"fmt"
	"github.com/fatih/color"
	"github.com/zeebo/bencode"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func decodetorrentfile(path string) (map[string]interface{}, error) {
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	var torrent map[string]interface{}
	if err := bencode.DecodeBytes([]byte(dat), &torrent); err != nil {
		log.Println(err)
		return nil, err
	}
	return torrent, nil
}

func encodetorrentfile(path string, newstructure map[string]interface{}) error {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		os.Create(path)
	}

	file, err := os.OpenFile(path, os.O_WRONLY, 0666)
	if err != nil {
		log.Println(err)
		return err
	}
	defer file.Close()
	bufferedWriter := bufio.NewWriter(file)
	enc := bencode.NewEncoder(bufferedWriter)
	if err := enc.Encode(newstructure); err != nil {
		log.Println(err)
		return err
	}
	bufferedWriter.Flush()
	return nil
}

func fixhasroot(path string, comChannel chan string) error {
	ext := filepath.Ext(path)
	torrentfilepath := path[0:len(path)-len(ext)] + ".torrent"
	_, fastresumename := filepath.Split(path)
	if _, err := os.Stat(torrentfilepath); os.IsNotExist(err) {
		comChannel <- fmt.Sprintf("Can't find torrent file for %v", path)
		return err
	}
	fastresume, err := decodetorrentfile(path)
	if err != nil {
		log.Println(err)
		comChannel <- fmt.Sprintf("Can't decode fastresume file %v", path)
		return err
	}
	torrentfile, err := decodetorrentfile(torrentfilepath)
	if err != nil {
		log.Println(err)
		comChannel <- fmt.Sprintf("Can't decode torrent file %v", torrentfilepath)
		return err
	}

	torrentname := torrentfile["info"].(map[string]interface{})["name"].(string)
	if _, ok := torrentfile["info"].(map[string]interface{})["files"]; ok == false {
		comChannel <- fmt.Sprintf("Torrent %v not has directory. Skipping", torrentname)
		return nil
	}
	origpath := fastresume["qBt-savePath"].(string)
	slashByte := []byte("\\")[0]
	if origpath[len(origpath)-1] == slashByte {
		origpath = origpath[:len(origpath)-1]
	}
	_, lastdirname := filepath.Split(strings.Replace(origpath, "\\", "/", -1))
	if lastdirname == torrentname {
		fastresume["qBt-hasRootFolder"] = 1
		fastresume["qBt-savePath"] = origpath[0 : len(origpath)-len(lastdirname)]
		if _, ok := fastresume["mapped_files"]; ok  {
			delete(fastresume, "mapped_files")
		}
		fastresume["save_path"] = fastresume["qBt-savePath"]
		fastresume["qBt-name"] = ""
		err = encodetorrentfile(path, fastresume)
		if err != nil {
			log.Println(err)
			comChannel <- fmt.Sprintf("Can't encode newtorrent %v (%v file)", torrentname, fastresumename)
			return err
		}
		comChannel <- fmt.Sprintf("Succesfully encoded new torrent %v (%v file)", torrentname, fastresumename)
		return nil
	}
	comChannel <- fmt.Sprintf("Nothing to do in torrent %v (%v file). Skipping", torrentname, fastresumename)
	return nil
}

func main() {
	directory := os.Getenv("LOCALAPPDATA") + "\\qBittorrent\\BT_backup\\"
	files, _ := filepath.Glob(directory + "*fastresume")
	_, err := os.Stat(directory)
	if err != nil {
		log.Println(err)
		time.Sleep(30 * time.Second)
		os.Exit(1)
	}
	color.HiRed("Check that the qBittorrent is turned off and the directory %v is backed up.\n\n", directory)
	fmt.Println("Press Enter to start")
	fmt.Scanln()
	comChannel := make(chan string, len(files))
	totaljobs := len(files)
	numjob := 1
	for _, file := range files {
		go fixhasroot(file, comChannel)
	}
	for message := range comChannel {
		fmt.Printf("%v/%v %v \n", numjob, totaljobs, message)
		if numjob == totaljobs {
			break
		}
		numjob++
	}
	fmt.Println("\nPress Enter to exit")
	fmt.Scanln()
}
